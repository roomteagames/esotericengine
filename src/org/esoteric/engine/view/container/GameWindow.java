package org.esoteric.engine.view.container;

import org.esoteric.engine.model.WindowModel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by russell_templet on 2/18/16.
 */
public class GameWindow extends JFrame {

    protected Dimension size;
    protected boolean resizable;
    protected boolean maintainAspectRatio;
    protected boolean exitOnClose;

    public GameWindow () {}

    /*public GameWindow(GameCanvas _panel, Dimension _size) {
        this(_panel, _size, true, false);
    }

    public GameWindow(GameCanvas _panel, Dimension _size, boolean _resizable, boolean _maintainAspectRatio) {
        build(_panel, _size, _resizable);
        maintainAspectRatio = _maintainAspectRatio;
    }

    private void build(GameCanvas panel, Dimension size, boolean resizable) {
        setSize(size);
        setResizable(resizable);
        init();

        //todo: is there a way to maintain aspect ratio on resize in swing???
        setContentPane(panel);
    }*/

    protected void init() {
        //todo: allow for additional settings in subclasses
    }

    @Override
    public void setSize(Dimension _size) {
        size = _size;
        super.setSize(_size);
    }

    @Override
    public void setResizable(boolean _resizable) {
        resizable = _resizable;
        super.setResizable(_resizable);
    }

    public void setMaintainAspectRatio(boolean _maintainAspectRatio) {
        maintainAspectRatio = _maintainAspectRatio;
        //todo: is there a way to maintain aspect ratio on resize in swing???
    }

    public void setExitOnClose(boolean _exitOnClose) {
        exitOnClose = _exitOnClose;
        super.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    @Override
    public Dimension getSize() {
        return size;
    }

    @Override
    public boolean isResizable() {
        return resizable;
    }

    public boolean isMaintainAspectRatio() {
        return maintainAspectRatio;
    }

    public boolean isExitOnClose() {
        return exitOnClose;
    }

    public void showWindow() {
        setVisible(true);
    }

    public void hideWindow() {
        setVisible(false);
    }
}
