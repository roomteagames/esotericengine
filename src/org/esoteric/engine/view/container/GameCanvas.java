package org.esoteric.engine.view.container;

import org.esoteric.engine.view.entity.EntityView;
import org.esoteric.engine.view.entity.drawing.TransformGraphics;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by russell_templet on 2/18/16.
 */
public class GameCanvas extends JComponent {

    protected Dimension size;
    protected Dimension resolution;

    public GameCanvas () {}

    /*public GameCanvas(Dimension _size, Dimension _resolution) {
        size = _size;
        resolution = _resolution;
        init();
    }*/

    protected void init() {

    }

    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;

        //todo: find a way to NOT create every draw
        //todo: find a way to pass screen transform from game code
        TransformGraphics tg = new TransformGraphics(graphics2D, /** temp */null);

        double widthRatio = size.getWidth() / resolution.getWidth();
        double heightRatio = size.getHeight() / resolution.getHeight();
        graphics2D.scale(widthRatio, heightRatio);

        render(tg);
    }

    protected void render(TransformGraphics g) {
        //TODO: put something here
    }

    @Override
    public void setSize(Dimension _size) {
        size = _size;
        super.setSize(_size);
    }

    public void setResolution(Dimension _resolution) {
        resolution = _resolution;
        //TODO: how do we set the resolution???
    }

    @Override
    public Dimension getSize() {
        return size;
    }

    public Dimension getResolution() {
        return resolution;
    }
}
