package org.esoteric.engine.view.entity.test;

import org.esoteric.commons.math.vector.Vector2;
import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.model.test.TestCharacter;
import org.esoteric.engine.view.entity.EntityView;
import org.esoteric.engine.view.entity.drawing.Drawable;
import org.esoteric.engine.view.entity.drawing.TransformGraphics;
import org.esoteric.engine.view.entity.drawing.Transformation;

import java.awt.*;

/**
 * Created by russelltemplet on 8/26/16.
 */
public class TestCharacterView extends EntityView {

    //TODO: use a generic so you can set the type of model
    public TestCharacterView (Entity _model, Drawable _drawable) {
        super(_model, _drawable);
    }

    @Override
    public void render(TransformGraphics g) {
        //reads model to determine view properties
        TestCharacter character = (TestCharacter) model;
        int redLevel = character.getAnger() * 5;

        //sets fields for transformation
        Vector2 position = new Vector2(100, 100);
        Dimension scale = new Dimension(1, 1);
        float rotation = 0;
        Color filter = new Color(redLevel, 0, 0);

        Transformation t = new Transformation(position, scale, rotation, filter);
        drawable.draw(g, t);
    }
}
