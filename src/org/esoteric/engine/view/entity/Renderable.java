package org.esoteric.engine.view.entity;

import org.esoteric.engine.view.entity.drawing.TransformGraphics;

/**
 * Created by russelltemplet on 6/26/16.
 */

@FunctionalInterface
public interface Renderable {

    void render(TransformGraphics g);
}
