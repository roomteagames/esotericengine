package org.esoteric.engine.view.entity;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.view.entity.drawing.Transformation;
import org.esoteric.engine.view.entity.drawing.Drawable;
import org.esoteric.engine.view.entity.drawing.TransformGraphics;

import java.awt.*;

/**
 * Created by russelltemplet on 7/13/16.
 *
 * Default View used to render entities without specific transformation cases
 * Calls a drawable without passing in any transformation information
 * Override for specific entities to create transformations based on certain model properties
 */
public class EntityView implements Renderable {

    protected Entity model;
    protected Drawable drawable;

    public EntityView (Entity _model, Drawable _drawable) {
        model = _model;
        drawable = _drawable;
    }

    public void render(TransformGraphics g) {
        drawable.draw(g, null);
    }
}
