package org.esoteric.engine.view.entity.drawing;

/**
 * Created by russelltemplet on 7/13/16.
 */
public interface Drawable {

    void draw(TransformGraphics g, Transformation transform);
}
