package org.esoteric.engine.view.entity.drawing;

import org.esoteric.commons.math.vector.Vector2;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Created by russelltemplet on 7/15/16.
 */
public class Transformation {

    private Vector2 position;
    private Dimension scale;
    private Float rotation;
    private Color colorFilter;
    private AffineTransform affine;

    public Transformation(Vector2 _position) {
        this(_position, new Dimension(1, 1));
    }

    public Transformation(Vector2 _position, Dimension _scale) {
        this(_position, _scale, 0f);
    }

    public Transformation(Vector2 _position, Dimension _scale, Float _rotation) {
        this(_position, _scale, _rotation, null);
    }

    public Transformation(Vector2 _position, Dimension _scale, Float _rotation, Color _colorFilter) {
        position = _position;
        scale = _scale;
        colorFilter = _colorFilter;
        rotation = _rotation;
        initAffine();
    }

    private void initAffine() {
        affine = new AffineTransform();
        affine.translate(position.getI(), position.getJ());
        affine.scale(scale.width, scale.height);
        affine.rotate(Math.toRadians(rotation));
    }

    public Vector2 getPosition() {
        return position;
    }

    public Dimension getScale() {
        return scale;
    }

    public Float getRotation() {
        return rotation;
    }

    public Color getColorFilter() {
        return colorFilter;
    }

    public AffineTransform getAffine() {
        return affine;
    }

    public Transformation combine(Transformation t) {
        Vector2 combinedPos = position.plus(t.getPosition());
        Dimension combinedScale = new Dimension(scale.width + t.getScale().width, scale.height + t.getScale().width);
        Float combinedRot = rotation + t.getRotation();
        Color combinedCol = blendColors(colorFilter, t.getColorFilter(), 0.5f);
        return new Transformation(combinedPos, combinedScale, combinedRot, combinedCol);
    }

    /**
     * adapted from Julius B., dARKpRINCE, and bmauter on StackOverflow
     * Appears to NOT be 100% accurate, but better than nothing so far...
     */
    public static Color blendColors(Color c1, Color c2, float ratio) {
        if ( ratio > 1f ) ratio = 1f;
        else if ( ratio < 0f ) ratio = 0f;
        float iRatio = 1.0f - ratio;

        int i1 = c1.getRGB();
        int i2 = c2.getRGB();

        int a1 = (i1 >> 24 & 0xff);
        int r1 = ((i1 & 0xff0000) >> 16);
        int g1 = ((i1 & 0xff00) >> 8);
        int b1 = (i1 & 0xff);

        int a2 = (i2 >> 24 & 0xff);
        int r2 = ((i2 & 0xff0000) >> 16);
        int g2 = ((i2 & 0xff00) >> 8);
        int b2 = (i2 & 0xff);

        int a = (int)((a1 * iRatio) + (a2 * ratio));
        int r = (int)((r1 * iRatio) + (r2 * ratio));
        int g = (int)((g1 * iRatio) + (g2 * ratio));
        int b = (int)((b1 * iRatio) + (b2 * ratio));

        return new Color( a << 24 | r << 16 | g << 8 | b, true);
    }
}
