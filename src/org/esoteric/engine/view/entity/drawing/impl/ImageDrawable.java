package org.esoteric.engine.view.entity.drawing.impl;

import org.esoteric.engine.view.entity.drawing.Transformation;
import org.esoteric.engine.view.entity.drawing.Drawable;
import org.esoteric.engine.view.entity.drawing.TransformGraphics;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


/**
 * Created by russelltemplet on 7/13/16.
 */
public class ImageDrawable implements Drawable {

    //todo: eventually make this work with a list of images for animation purposes
    private BufferedImage image;

    public ImageDrawable (String imageLocation) throws IOException {
        File file = new File(imageLocation);
        image = ImageIO.read(file);
    }

    @Override
    public void draw(TransformGraphics g, Transformation transform) {
        //todo: how do you control image dimensions? -- do we just assume image is the correct dimension?
        g.drawImage(image, 0, 0, transform);
    }
}
