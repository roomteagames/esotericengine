package org.esoteric.engine.view.entity.drawing;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by russelltemplet on 7/15/16.
 */
public class TransformGraphics {

    private Graphics2D g;
    private Transformation screenTransform;

    public TransformGraphics(Graphics2D _g) {
        this(_g, null);
    }

    public TransformGraphics(Graphics2D _g, Transformation _screenTransform) {
        g = _g;
        screenTransform = _screenTransform;
    }

    //todo: add more wrapper methods if need be

    public void drawLine(int x1, int y1, int x2, int y2) {
        drawLine(x1, y1, x2, y2, null);
    }

    public void drawLine(int x1, int y1, int x2, int y2, Transformation transform) {
        transformGraphics(transform);
        g.drawLine(x1, y1, x2, y2);
    }

    public void drawShape(Shape shape) {
        drawShape(shape, null);
    }

    public void drawShape(Shape shape, Transformation transform) {
        transformGraphics(transform);
        g.draw(shape);
    }

    public void fillShape(Shape shape) {
        fillShape(shape, null);
    }

    public void fillShape(Shape shape, Transformation transform) {
        transformGraphics(transform);
        g.fill(shape);
    }

    public void drawString(String str, int xPos, int yPos) {
        drawString(str, xPos, yPos, null);
    }

    public void drawString(String str, int xPos, int yPos, Transformation transform) {
        transformGraphics(transform);
        g.drawString(str, xPos, yPos);
    }

    public void drawString(String str, Font font, int xPos, int yPos) {
        g.setFont(font);
        drawString(str, xPos, yPos);
    }

    public void drawString(String str, Font font, int xPos, int yPos, Transformation transform) {
        g.setFont(font);
        drawString(str, xPos, yPos, transform);
    }

    public void drawImage(BufferedImage img, int xPos, int yPos) {
        drawImage(img, xPos, yPos, null);
    }

    //todo: applying a color filter to an image might be difficult -- does not currently work
    public void drawImage(BufferedImage img, int xPos, int yPos, Transformation transform) {
        transformGraphics(transform);
        g.drawImage(img, xPos, yPos, null); //todo: perhaps look into ImageObserver, and whether it's needed
    }

    private void transformGraphics(Transformation input) {
        Transformation t = getTransformation(input);
        if (t != null) {
            g.transform(t.getAffine());
            g.setColor(t.getColorFilter());
        }
    }

    //todo: convenient code, but requires 2 null checks every time -- clean up eventually
    private Transformation getTransformation(Transformation objTransform) {
        Transformation t;
        if (screenTransform != null) {
            g.setColor(screenTransform.getColorFilter());
            if (objTransform != null) {
                t = screenTransform.combine(objTransform);
            } else {
                t = screenTransform;
            }
        } else {
            t = objTransform;
        }
        return t;
    }
}
