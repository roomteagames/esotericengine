package org.esoteric.engine.view.entity.drawing.test;

import org.esoteric.engine.view.entity.drawing.Drawable;
import org.esoteric.engine.view.entity.drawing.TransformGraphics;
import org.esoteric.engine.view.entity.drawing.Transformation;

import java.awt.*;

/**
 * Created by russelltemplet on 8/27/16.
 */
public class TestCharacterDrawable implements Drawable {

    @Override
    public void draw(TransformGraphics g, Transformation transform) {
        Dimension size = new Dimension(100, 100);
        Shape rect = new Rectangle(size);
        g.fillShape(rect, transform);
    }
}
