package org.esoteric.engine.view.config;

import java.util.List;

/**
 * Created by russelltemplet on 8/3/16.
 */
public class ViewConfigList {

    private List<ViewConfig> views;

    public List<ViewConfig> getViews() {
        return views;
    }

    public void setViews(List<ViewConfig> views) {
        this.views = views;
    }
}
