package org.esoteric.engine.view.config;

import java.io.Serializable;

/**
 * Created by russelltemplet on 8/3/16.
 */
public class ViewConfig implements Serializable {

    private String modelClass;
    private String viewClass;

    public String getModelClass() {
        return modelClass;
    }

    public String getViewClass() {
        return viewClass;
    }

    public void setModelClass(String modelClass) {
        this.modelClass = modelClass;
    }

    public void setViewClass(String viewClass) {
        this.viewClass = viewClass;
    }
}
