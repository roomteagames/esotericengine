package org.esoteric.engine.model;

import org.esoteric.commons.model.BaseModel;

/**
 * Created by russelltemplet on 7/12/16.
 */
public class InputModel extends BaseModel {

    private boolean directionalDownPressed;
    private boolean directionalUpPressed;
    private boolean directionalLeftPressed;
    private boolean directionalRightPressed;

    private boolean mainFacePressed;
    private boolean topFacePressed;
    private boolean leftFacePressed;
    private boolean rightFacePressed;

    private boolean leftButtonPressed;
    private boolean rightButtonPressed;
    private boolean leftTriggerPressed;
    private boolean rightTriggerPressed;

    private boolean selectPressed;
    private boolean startPressed;

    private boolean leftStickPressed;
    private boolean rightStickPressed;

    private boolean auxilliary1Pressed;
    private boolean auxilliary2Pressed;
    private boolean auxilliary3Pressed;
    private boolean auxilliary4Pressed;
    private boolean auxilliary5Pressed;

    //TODO: add analog stick support


    public boolean isDirectionalDownPressed() {
        return directionalDownPressed;
    }

    public void setDirectionalDownPressed(boolean directionalDownPressed) {
        this.directionalDownPressed = directionalDownPressed;
    }

    public boolean isDirectionalUpPressed() {
        return directionalUpPressed;
    }

    public void setDirectionalUpPressed(boolean directionalUpPressed) {
        this.directionalUpPressed = directionalUpPressed;
    }

    public boolean isDirectionalLeftPressed() {
        return directionalLeftPressed;
    }

    public void setDirectionalLeftPressed(boolean directionalLeftPressed) {
        this.directionalLeftPressed = directionalLeftPressed;
    }

    public boolean isDirectionalRightPressed() {
        return directionalRightPressed;
    }

    public void setDirectionalRightPressed(boolean directionalRightPressed) {
        this.directionalRightPressed = directionalRightPressed;
    }

    public boolean isMainFacePressed() {
        return mainFacePressed;
    }

    public void setMainFacePressed(boolean mainFacePressed) {
        this.mainFacePressed = mainFacePressed;
    }

    public boolean isTopFacePressed() {
        return topFacePressed;
    }

    public void setTopFacePressed(boolean topFacePressed) {
        this.topFacePressed = topFacePressed;
    }

    public boolean isLeftFacePressed() {
        return leftFacePressed;
    }

    public void setLeftFacePressed(boolean leftFacePressed) {
        this.leftFacePressed = leftFacePressed;
    }

    public boolean isRightFacePressed() {
        return rightFacePressed;
    }

    public void setRightFacePressed(boolean rightFacePressed) {
        this.rightFacePressed = rightFacePressed;
    }

    public boolean isLeftButtonPressed() {
        return leftButtonPressed;
    }

    public void setLeftButtonPressed(boolean leftButtonPressed) {
        this.leftButtonPressed = leftButtonPressed;
    }

    public boolean isRightButtonPressed() {
        return rightButtonPressed;
    }

    public void setRightButtonPressed(boolean rightButtonPressed) {
        this.rightButtonPressed = rightButtonPressed;
    }

    public boolean isLeftTriggerPressed() {
        return leftTriggerPressed;
    }

    public void setLeftTriggerPressed(boolean leftTriggerPressed) {
        this.leftTriggerPressed = leftTriggerPressed;
    }

    public boolean isRightTriggerPressed() {
        return rightTriggerPressed;
    }

    public void setRightTriggerPressed(boolean rightTriggerPressed) {
        this.rightTriggerPressed = rightTriggerPressed;
    }

    public boolean isSelectPressed() {
        return selectPressed;
    }

    public void setSelectPressed(boolean selectPressed) {
        this.selectPressed = selectPressed;
    }

    public boolean isStartPressed() {
        return startPressed;
    }

    public void setStartPressed(boolean startPressed) {
        this.startPressed = startPressed;
    }

    public boolean isLeftStickPressed() {
        return leftStickPressed;
    }

    public void setLeftStickPressed(boolean leftStickPressed) {
        this.leftStickPressed = leftStickPressed;
    }

    public boolean isRightStickPressed() {
        return rightStickPressed;
    }

    public void setRightStickPressed(boolean rightStickPressed) {
        this.rightStickPressed = rightStickPressed;
    }

    public boolean isAuxilliary1Pressed() {
        return auxilliary1Pressed;
    }

    public void setAuxilliary1Pressed(boolean auxilliary1Pressed) {
        this.auxilliary1Pressed = auxilliary1Pressed;
    }

    public boolean isAuxilliary2Pressed() {
        return auxilliary2Pressed;
    }

    public void setAuxilliary2Pressed(boolean auxilliary2Pressed) {
        this.auxilliary2Pressed = auxilliary2Pressed;
    }

    public boolean isAuxilliary3Pressed() {
        return auxilliary3Pressed;
    }

    public void setAuxilliary3Pressed(boolean auxilliary3Pressed) {
        this.auxilliary3Pressed = auxilliary3Pressed;
    }

    public boolean isAuxilliary4Pressed() {
        return auxilliary4Pressed;
    }

    public void setAuxilliary4Pressed(boolean auxilliary4Pressed) {
        this.auxilliary4Pressed = auxilliary4Pressed;
    }

    public boolean isAuxilliary5Pressed() {
        return auxilliary5Pressed;
    }

    public void setAuxilliary5Pressed(boolean auxilliary5Pressed) {
        this.auxilliary5Pressed = auxilliary5Pressed;
    }

    public boolean consumeDirectionalDownPress() {
        boolean value = directionalDownPressed;
        directionalDownPressed = false;
        return value;
    }

    public boolean consumeDirectionalUpPress() {
        boolean value = directionalUpPressed;
        directionalUpPressed = false;
        return value;
    }

    public boolean consumeDirectionalLeftPress() {
        boolean value = directionalLeftPressed;
        directionalLeftPressed = false;
        return value;
    }

    public boolean consumeDirectionalRightPress() {
        boolean value = directionalRightPressed;
        directionalRightPressed = false;
        return value;
    }

    public boolean consumeMainFacePress() {
        boolean value = mainFacePressed;
        mainFacePressed = false;
        return value;
    }

    public boolean consumeTopFacePress() {
        boolean value = topFacePressed;
        topFacePressed = false;
        return value;
    }

    public boolean consumeLeftFacePress() {
        boolean value = leftFacePressed;
        leftFacePressed = false;
        return value;
    }

    public boolean consumeRightPress() {
        boolean value = rightFacePressed;
        rightFacePressed = false;
        return value;
    }

    public boolean consumeLeftButtonPress() {
        boolean value = leftButtonPressed;
        leftButtonPressed = false;
        return value;
    }

    public boolean consumeRightButtonPress() {
        boolean value = rightButtonPressed;
        rightButtonPressed = false;
        return value;
    }

    public boolean consumeLeftTriggerPress() {
        boolean value = leftTriggerPressed;
        leftTriggerPressed = false;
        return value;
    }

    public boolean consumeRightTriggerPress() {
        boolean value = rightTriggerPressed;
        rightTriggerPressed = false;
        return value;
    }

    public boolean consumeSelectPress() {
        boolean value = selectPressed;
        selectPressed = false;
        return value;
    }

    public boolean consumeStartPress() {
        boolean value = startPressed;
        startPressed = false;
        return value;
    }

    public boolean consumeLeftStickPress() {
        boolean value = leftStickPressed;
        leftStickPressed = false;
        return value;
    }

    public boolean consumeRightStickPress() {
        boolean value = rightStickPressed;
        rightStickPressed = false;
        return value;
    }

    public boolean consumeAuxilliary1Press() {
        boolean value = auxilliary1Pressed;
        auxilliary1Pressed = false;
        return value;
    }

    public boolean consumeAuxilliary2Press() {
        boolean value = auxilliary2Pressed;
        auxilliary2Pressed = false;
        return value;
    }

    public boolean consumeAuxilliary3Press() {
        boolean value = auxilliary3Pressed;
        auxilliary3Pressed = false;
        return value;
    }

    public boolean consumeAuxilliary4Press() {
        boolean value = auxilliary4Pressed;
        auxilliary4Pressed = false;
        return value;
    }

    public boolean consumeAuxilliary5Press() {
        boolean value = auxilliary5Pressed;
        auxilliary5Pressed = false;
        return value;
    }

}
