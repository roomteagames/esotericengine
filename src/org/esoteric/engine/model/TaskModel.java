package org.esoteric.engine.model;

import org.esoteric.commons.model.BaseModel;
import org.esoteric.engine.model.task.GameTask;
import org.esoteric.engine.model.task.impl.RenderTask;
import org.esoteric.engine.model.task.impl.UpdateTask;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by russelltemplet on 7/12/16.
 */
public class TaskModel extends BaseModel {

    private UpdateTask updateTask;
    private RenderTask renderTask;
    private Map<String, GameTask> auxiliaryTaskMap;

    public TaskModel () {
        auxiliaryTaskMap = new HashMap<>();
    }

    public TaskModel (UpdateTask _updateTask, RenderTask _renderTask) {
        this();
        updateTask = _updateTask;
        renderTask = _renderTask;
    }

    public void addAuxiliaryTask(String key, GameTask task) {
        auxiliaryTaskMap.put(key, task);
    }

    public void addAuxiliaryTaskByName(GameTask task) {
        String name = task.getThreadName();
        auxiliaryTaskMap.put(name, task);
    }

    public void setUpdateTask(UpdateTask _updateTask) {
        updateTask = _updateTask;
    }

    public void setRenderTask(RenderTask _renderTask) {
        renderTask = _renderTask;
    }

    public UpdateTask getUpdateTask() {
        return updateTask;
    }

    public RenderTask getRenderTask() {
        return renderTask;
    }

    public GameTask getAuxiliaryTask(String name) {
        return auxiliaryTaskMap.get(name);
    }

    public Collection<GameTask> getAuxiliaryTasks() {
        return auxiliaryTaskMap.values();
    }
}
