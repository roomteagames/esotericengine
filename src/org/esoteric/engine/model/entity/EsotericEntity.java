package org.esoteric.engine.model.entity;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.model.InputModel;

/**
 * Created by russelltemplet on 8/26/16.
 */
public abstract class EsotericEntity extends Entity {

    protected InputModel inputModel;

    public EsotericEntity (InputModel _inputModel) {
        inputModel = _inputModel;
    }


}
