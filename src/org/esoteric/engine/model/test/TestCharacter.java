package org.esoteric.engine.model.test;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.model.InputModel;
import org.esoteric.engine.model.entity.EsotericEntity;

/**
 * Created by russelltemplet on 8/26/16.
 */
public class TestCharacter extends EsotericEntity {

    private int anger;
    private int shyness;
    private int fullness;

    public TestCharacter(InputModel _inputModel) {
        super(_inputModel);
    }

    public TestCharacter(InputModel _inputModel, int _anger, int _shyness, int _fullness) {
        super(_inputModel);
        anger = _anger;
        shyness = _shyness;
        fullness = _fullness;
    }

    @Override
    public void update() {
        if (inputModel.consumeDirectionalUpPress()) {
            anger++;
        }
    }

    public int getAnger() {
        return anger;
    }

    public int getShyness() {
        return shyness;
    }

    public int getFullness() {
        return fullness;
    }
}
