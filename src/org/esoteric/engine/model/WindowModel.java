package org.esoteric.engine.model;

import org.esoteric.commons.model.BaseModel;
import org.esoteric.engine.view.container.GameCanvas;
import org.esoteric.engine.view.container.GameWindow;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class WindowModel extends BaseModel {

    private GameWindow window;
    private GameCanvas canvas;

    public WindowModel () {
        window = new GameWindow();
        canvas = new GameCanvas();
    }

    public GameWindow getWindow() {
        return window;
    }

    public GameCanvas getCanvas() {
        return canvas;
    }
}
