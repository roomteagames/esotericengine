package org.esoteric.engine.model.config;

import org.esoteric.commons.model.BaseModel;

import java.util.Collection;

/**
 * Created by russelltemplet on 8/27/16.
 */
public interface Config<T, U extends BaseModel> {

    /**
     * T param is type of collection or map used to store the config data
     *  Contents of the collection are expected to be lambdas, especially Consumers or BiConsumers
     * U is type of model that config is used to set
     *
     */

    T getAll();
}
