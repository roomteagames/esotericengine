package org.esoteric.engine.model.config;

import org.esoteric.engine.model.WindowModel;

import java.awt.*;
import java.util.function.Consumer;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class WindowConfig extends ListConfig<Consumer<WindowModel>, WindowModel> {

    public void showCanvas(boolean isShown) {
        add(m ->
                m.getCanvas()
                        .setVisible(isShown)
        );
    }

    public void canvasSize(Dimension size) {
        add(m ->
                m.getCanvas()
                        .setSize(size)
        );
    }

    public void canvasResolution(Dimension resolution) {
        add(m ->
                m.getCanvas()
                        .setResolution(resolution)
        );
    }

    public void windowSize(Dimension size) {
        add(m ->
                m.getWindow()
                        .setSize(size)
        );
    }

    public void windowResizable(boolean isResizable) {
        add(m ->
                m.getWindow()
                        .setResizable(isResizable)
        );
    }

    public void showWindow(boolean isShown) {
        add(m ->
                m.getWindow()
                        .setVisible(isShown)
        );
    }

    public void maintainAspectRatio(boolean isMaintained) {
        add(m ->
                m.getWindow()
                        .setMaintainAspectRatio(isMaintained)
        );
    }

    public void exitOnClose(boolean isExit) {
        add(m ->
                m.getWindow()
                        .setExitOnClose(isExit)
        );
    }

    //TODO: add more if necessary
}
