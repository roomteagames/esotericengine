package org.esoteric.engine.model.config;

import org.esoteric.engine.model.InputModel;

import java.util.function.BiConsumer;

/**
 * Created by russelltemplet on 8/27/16.
 */
public class InputConfig extends MapConfig<Integer, BiConsumer<InputModel, Boolean>, InputModel> {

    public void directionalDown(int value) {
        add(value, InputModel::setDirectionalDownPressed);
    }

    public void directionalUp(int value) {
        add(value, InputModel::setDirectionalUpPressed);
    }

    public void directionalLeft(int value) {
        add(value, InputModel::setDirectionalLeftPressed);
    }

    public void directionalRight(int value) {
        add(value, InputModel::setDirectionalRightPressed);
    }

    public void mainFace(int value) {
        add(value, InputModel::setMainFacePressed);
    }

    public void topFace(int value) {
        add(value, InputModel::setTopFacePressed);
    }

    public void leftFace(int value) {
        add(value, InputModel::setLeftFacePressed);
    }

    public void rightFace(int value) {
        add(value, InputModel::setRightFacePressed);
    }

    public void leftButton(int value) {
        add(value, InputModel::setLeftButtonPressed);
    }

    public void rightButton(int value) {
        add(value, InputModel::setRightButtonPressed);
    }

    public void leftTrigger(int value) {
        add(value, InputModel::setLeftTriggerPressed);
    }

    public void rightTrigger(int value) {
        add(value, InputModel::setRightTriggerPressed);
    }

    public void setStart(int value) {
        add(value, InputModel::setStartPressed);
    }

    public void setSelect(int value) {
        add(value, InputModel::setSelectPressed);
    }

    public void leftStickButton(int value) {
        add(value, InputModel::setLeftStickPressed);
    }

    public void rightStickButton(int value) {
        add(value, InputModel::setRightStickPressed);
    }

    public void auxiliary1(int value) {
        add(value, InputModel::setAuxilliary1Pressed);
    }

    public void auxiliary2(int value) {
        add(value, InputModel::setAuxilliary2Pressed);
    }

    public void auxiliary3(int value) {
        add(value, InputModel::setAuxilliary3Pressed);
    }

    public void auxiliary4(int value) {
        add(value, InputModel::setAuxilliary4Pressed);
    }

    public void auxiliary5(int value) {
        add(value, InputModel::setAuxilliary5Pressed);
    }
}
