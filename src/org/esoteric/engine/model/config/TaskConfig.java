package org.esoteric.engine.model.config;

import org.esoteric.engine.model.TaskModel;
import org.esoteric.engine.model.task.GameTask;
import org.esoteric.engine.model.task.impl.RenderTask;
import org.esoteric.engine.model.task.impl.UpdateTask;

import java.util.function.Consumer;

/**
 * Created by russelltemplet on 8/28/16.
 */
public class TaskConfig extends ListConfig<Consumer<TaskModel>, TaskModel> {

    public void addGameTasks(int updateFrameRate, int renderFrameRate) {
        UpdateTask updateTask = new UpdateTask(updateFrameRate);
        add(m ->
                m.setUpdateTask(updateTask)
        );

        RenderTask renderTask = new RenderTask(renderFrameRate);
        add(m ->
                m.setRenderTask(renderTask)
        );
    }

    //TODO: provide a future method to add tasks to config
}