package org.esoteric.engine.model.config;

import org.esoteric.commons.model.BaseModel;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by russelltemplet on 8/28/16.
 */
public abstract class ListConfig<E extends Consumer<U>, U extends BaseModel> implements Config<List<E>, U> {

    protected List<E> configList;

    public ListConfig () {
        configList = new ArrayList<>();
    }

    public void add(E element) {
        configList.add(element);
    }

    public E get(int index) {
        return configList.get(index);
    }

    @Override
    public List<E> getAll() {
        return configList;
    }

    public void applyAll(U model) {
        configList.forEach(e ->
                e.accept(model)
        );
    }
}
