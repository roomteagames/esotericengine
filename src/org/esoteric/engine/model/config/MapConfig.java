package org.esoteric.engine.model.config;

import org.esoteric.commons.model.BaseModel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by russelltemplet on 8/28/16.
 */
public abstract class MapConfig<K, V, U extends BaseModel> implements Config<Map<K, V>, U> {

    protected Map<K, V> configMap;

    public MapConfig() {
        configMap = new HashMap<>();
    }

    public void add(K key, V value) {
        configMap.put(key, value);
    }

    public V get(K key) {
        return configMap.get(key);
    }

    @Override
    public Map<K, V> getAll() {
        return configMap;
    }
}
