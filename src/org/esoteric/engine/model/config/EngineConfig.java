package org.esoteric.engine.model.config;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by russelltemplet on 9/4/16.
 */
public class EngineConfig {

    private EntityConfig entityConfig;
    private InputConfig inputConfig;
    private TaskConfig taskConfig;
    private WindowConfig windowConfig;

    public EngineConfig () {
        this(null, null);
    }

    public EngineConfig (EntityConfig _entityConfig, InputConfig _inputConfig) {
        this(_entityConfig, _inputConfig, null);
    }

    public EngineConfig (EntityConfig _entityConfig, InputConfig _inputConfig, WindowConfig _windowConfig) {
        this(_entityConfig, _inputConfig, _windowConfig, null);
    }

    public EngineConfig (EntityConfig _entityConfig, InputConfig _inputConfig, WindowConfig _windowConfig, TaskConfig _taskConfig) {
        entityConfig = _entityConfig;
        inputConfig = _inputConfig;
        taskConfig = _taskConfig;
        windowConfig = _windowConfig;

        generateDefaults();
    }

    private void generateDefaults() {
        if (entityConfig == null) {
            entityConfig = generateDefaultEntityConfig();
        }
        if (inputConfig == null) {
            inputConfig = generateDefaultInputConfig();
        }
        if (taskConfig == null) {
            taskConfig = generateDefaultTaskConfig();
        }
        if (windowConfig == null) {
            windowConfig = generateDefaultWindowConfig();
        }
    }

    private EntityConfig generateDefaultEntityConfig() {
        EntityConfig entityConfig = new EntityConfig();

        //TODO: set defaults
        return entityConfig;
    }

    private InputConfig generateDefaultInputConfig() {
        InputConfig config = new InputConfig();

        config.directionalUp(KeyEvent.VK_W);
        config.directionalLeft(KeyEvent.VK_A);
        config.directionalDown(KeyEvent.VK_S);
        config.directionalRight(KeyEvent.VK_D);

        config.mainFace(KeyEvent.VK_L);
        config.leftFace(KeyEvent.VK_K);
        config.topFace(KeyEvent.VK_O);
        config.rightFace(KeyEvent.VK_SEMICOLON);

        config.setStart(KeyEvent.VK_ESCAPE);
        config.setSelect(KeyEvent.VK_ENTER);

        return config;
    }

    private TaskConfig generateDefaultTaskConfig() {
        TaskConfig config = new TaskConfig();

        config.addGameTasks(100, 30);

        return config;
    }

    private WindowConfig generateDefaultWindowConfig() {
        WindowConfig config = new WindowConfig();

        config.windowSize(new Dimension(500, 500));
        config.windowResizable(true);
        config.exitOnClose(true);
        config.canvasSize(new Dimension(500, 500));
        config.showWindow(true);
        config.showCanvas(true);

        return config;
    }

    public EntityConfig getEntityConfig() {
        return entityConfig;
    }

    public InputConfig getInputConfig() {
        return inputConfig;
    }

    public TaskConfig getTaskConfig() {
        return taskConfig;
    }

    public WindowConfig getWindowConfig() {
        return windowConfig;
    }
}
