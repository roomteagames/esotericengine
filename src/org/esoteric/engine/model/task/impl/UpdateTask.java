package org.esoteric.engine.model.task.impl;

import org.esoteric.engine.model.task.FrameCatchUpTask;

/**
 * Created by russelltemplet on 7/12/16.
 */
public class UpdateTask extends FrameCatchUpTask {

    //TODO: put Room obj here???

    public UpdateTask (int _targetFrameRate) {
        super(_targetFrameRate, "UpdateTask");
    }

    @Override
    protected void runStartup() {

    }

    @Override
    protected void runTask() {

    }

    @Override
    protected void runShutdown() {

    }
}
