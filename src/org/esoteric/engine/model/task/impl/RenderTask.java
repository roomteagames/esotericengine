package org.esoteric.engine.model.task.impl;

import org.esoteric.engine.model.task.FrameBeginTask;
import org.esoteric.engine.view.entity.EntityView;

import java.util.List;

/**
 * Created by russelltemplet on 6/26/16.
 */
public class RenderTask extends FrameBeginTask {

    protected List<EntityView> entityViews; //TODO: perhaps the same grid system should be used (as in Room)

    public RenderTask (int _targetFrameRate) {
        super(_targetFrameRate, "RenderTask");
    }

    @Override
    protected void runStartup() {

    }

    @Override
    protected void runTask() {

    }

    @Override
    protected void runShutdown() {

    }
}
