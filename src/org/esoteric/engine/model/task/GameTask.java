package org.esoteric.engine.model.task;

/**
 * Created by russelltemplet on 7/12/16.
 */
public abstract class GameTask implements Runnable {

    protected String threadName;
    protected Thread thread;
    protected Long startTime;

    public GameTask() {
        this("GameTask-" + System.currentTimeMillis());
    }

    public GameTask(String _threadName) {
        threadName = _threadName;
        thread = new Thread(this, threadName);
    }

    @Override
    public void run() {
        try {
            startTime = System.currentTimeMillis();
            runStartup();
            runBody();
        } catch (InterruptedException e) {
            System.out.println("Killed task: " + threadName);
        }

    }

    protected void runBody() throws InterruptedException {
        runTask();
        runAfterBody();
    }

    protected void runAfterBody() throws InterruptedException {
        runShutdown();
    }

    protected void runStartup() throws InterruptedException { }

    protected abstract void runTask() throws InterruptedException;

    protected void runShutdown() throws InterruptedException { }

    public Long getStartTime() {
        return startTime;
    }

    public String getThreadName() {
        return threadName;
    }

    public Thread getThread() {
        return thread;
    }
}
