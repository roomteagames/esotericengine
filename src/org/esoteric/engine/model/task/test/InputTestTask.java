package org.esoteric.engine.model.task.test;

import org.esoteric.engine.model.InputModel;
import org.esoteric.engine.model.task.GameTask;

import javax.swing.*;
import java.awt.*;

/**
 * Created by russelltemplet on 7/13/16.
 */
public class InputTestTask extends GameTask {

    private InputModel model;

    public InputTestTask(InputModel _model) {
        super("InputTestTask");
        model = _model;
    }

    @Override
    protected void runStartup() {
        JPanel panel = new JPanel() {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
//                g.drawString("" + model.getPressedKeys(), 100, 100);
            }
        };
    }

    @Override
    protected void runTask() {

    }
}
