package org.esoteric.engine.model.task.test;

import org.esoteric.engine.view.entity.drawing.Transformation;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by russelltemplet on 7/31/16.
 */
public class TransformTestTask extends VisualTestTask {

    private double posX, posY;
    private double rotation;
    private int rotationDirection;
    private int elevationDirection;

    public TransformTestTask(int _targetFrameRate) {
        super(_targetFrameRate);
        posX = 150;
        posY = 150;
        rotationDirection = 0;
        elevationDirection = 0;
    }

    @Override
    protected void runStartup() {
        super.runStartup();
        SwingUtilities.invokeLater(this::addKeyListener);
    }

    private void addKeyListener() {
        System.out.println(Thread.currentThread().getName());
        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int code = e.getKeyCode();
                switch (code) {
                    case KeyEvent.VK_LEFT:
                        rotationDirection = -1;
                        break;
                    case KeyEvent.VK_RIGHT:
                        rotationDirection = 1;
                        break;
                    case KeyEvent.VK_UP:
                        elevationDirection = -1;
                        break;
                    case KeyEvent.VK_DOWN:
                        elevationDirection = 1;
                        break;
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {
                elevationDirection = 0;
                rotationDirection = 0;
            }
        });
    }

    @Override
    protected void render(Graphics2D g) {

    }

    @Override
    protected void runTask() {
//        posY += elevationDirection * 4;
        rotation += /*rotationDirection **/ 4;
        window.repaint();
    }
}
