package org.esoteric.engine.model.task.test;

import org.esoteric.engine.model.task.FrameBeginTask;

import javax.swing.*;
import java.awt.*;

/**
 * Created by russelltemplet on 6/25/16.
 */
public class VisualTestTask extends FrameBeginTask {

    static int WINDOW_WIDTH = 500;
    static int WINDOW_HEIGHT = 300;
    static int BAND_WIDTH = 10;

    private int xLoc;
    private int pixelsPerFrame;
    private boolean isIncreasing;
    private Rectangle rect;

    protected JFrame window;
    protected JPanel panel;

    public VisualTestTask(int _targetFrameRate) {
        this(_targetFrameRate, 1);
        rect = new Rectangle(0, 0, BAND_WIDTH, WINDOW_HEIGHT);
    }

    public VisualTestTask(int _targetFrameRate, int _pixelsPerFrame) {
        super(_targetFrameRate);
        initFields(_pixelsPerFrame);
    }

    public VisualTestTask(int _targetFrameRate, String _threadName) {
        this(_targetFrameRate, 1, _threadName);
    }

    public VisualTestTask(int _targetFrameRate, int _pixelsPerFrame, String _threadName) {
        super(_targetFrameRate, _threadName);
        initFields(_pixelsPerFrame);
    }

    private void initFields(int _pixelsPerFrame) {
        xLoc = 0;
        pixelsPerFrame = _pixelsPerFrame;
        isIncreasing = true;
        window = new JFrame();
    }

    @Override
    protected void runStartup() {
        panel = new JPanel() {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                render((Graphics2D) g);
            }
        };

        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setContentPane(panel);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setVisible(true);
    }

    protected void render(Graphics2D g) {
        g.fill(rect);
    }

    @Override
    protected void runTask() {
        if (isIncreasing) {
            if (xLoc + BAND_WIDTH < WINDOW_WIDTH) {
                xLoc += pixelsPerFrame;
            } else {
                isIncreasing = false;
            }
        } else {
            if (xLoc > 0) {
                xLoc -= pixelsPerFrame;
            } else {
                isIncreasing = true;
            }
        }
        window.repaint();
    }

    @Override
    protected void runShutdown() {

    }
}
