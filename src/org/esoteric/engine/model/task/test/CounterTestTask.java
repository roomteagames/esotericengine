package org.esoteric.engine.model.task.test;

import org.esoteric.engine.model.task.FrameBeginTask;

/**
 * Created by russell_templet on 2/17/16.
 */
public class CounterTestTask extends FrameBeginTask {

    private long numIterations;
    private Long previousTime;
    private Long max;
    private Long min;

    public CounterTestTask(int _targetFrameRate, long _numIterations) {
        super(_targetFrameRate);

        numIterations = _numIterations;
        previousTime = null;
        max = Long.MIN_VALUE;
        min = Long.MAX_VALUE;
    }

    @Override
    protected void runStartup() {
        System.out.println("Expected: " + targetFrameTime);
        System.out.println("Counting " + numIterations + " frames...");
    }

    @Override
    protected void runTask() {
        long timeMillis = System.currentTimeMillis();
        if (previousTime != null) {
            long difference = timeMillis - previousTime;
            updateMinMax(difference);
        }
        previousTime = timeMillis;

        if (iterationCount == numIterations) {
            setTerminated(true);
        }
    }

    private void updateMinMax(long difference) {
        if (difference < min) {
            min = difference;
        } else if (difference > max) {
            max = difference;
        }
    }

    @Override
    protected void runShutdown() {
        System.out.println("Error: " + (max - min) + " ms, min: " + min + ", max: " + max);
    }
}
