package org.esoteric.engine.model.task.test;

import org.esoteric.engine.model.task.FrameCatchUpTask;

import kuusisto.tinysound.TinySound;
import kuusisto.tinysound.Sound;

import java.io.File;

/**
 * Created by russell_templet on 2/17/16.
 */
public class SoundTestTask extends FrameCatchUpTask {

    public static final String defaultPath = "/Users/russell_templet/Dropbox/Esoteric/soundfx/walk.wav";

    private String soundPath;
    private Sound sound;

    public SoundTestTask (int _targetFrameRate) {
        this(defaultPath, _targetFrameRate);
    }


    public SoundTestTask(String _soundPath, int _targetFrameRate) {
        super(_targetFrameRate);
        soundPath = _soundPath;

        TinySound.init();
        sound = TinySound.loadSound(new File(_soundPath));
    }

    @Override
    protected void runTask() {
        sound.play();
        if (iterationCount > 10) {
            terminated = true;
        }
    }

    @Override
    protected void runShutdown() {
        TinySound.shutdown();
    }

}
