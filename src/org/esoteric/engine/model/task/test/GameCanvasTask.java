package org.esoteric.engine.model.task.test;

import org.esoteric.engine.controller.impl.InputController;
import org.esoteric.engine.model.InputModel;
import org.esoteric.engine.model.config.InputConfig;
import org.esoteric.engine.model.task.FrameBeginTask;
import org.esoteric.engine.model.test.TestCharacter;
import org.esoteric.engine.view.container.GameCanvas;
import org.esoteric.engine.view.container.GameWindow;
import org.esoteric.engine.view.entity.drawing.Drawable;
import org.esoteric.engine.view.entity.drawing.test.TestCharacterDrawable;
import org.esoteric.engine.view.entity.test.TestCharacterView;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by russelltemplet on 8/26/16.
 */
public class GameCanvasTask extends FrameBeginTask {

    public GameCanvasTask(int _frameRate) {
        super(_frameRate);
    }

    @Override
    protected void runStartup() {

        //TODO: move all this into an engine/controller eventually
        Dimension size = new Dimension(500, 500);
        Dimension resolution = new Dimension(500, 500);

//        GameCanvas canvas = new GameCanvas(size, resolution);
//        GameWindow window = new GameWindow(canvas, size, resolution);
//        window.showWindow();

        InputConfig inputConfig = new InputConfig();
        inputConfig.directionalUp(KeyEvent.VK_UP);

        InputModel inputModel = new InputModel();
//        InputController inputController = new InputController(inputModel, inputConfig, canvas);
//        inputController.start();

        TestCharacter character = new TestCharacter(inputModel);
        Drawable characterDrawable = new TestCharacterDrawable();
        TestCharacterView characterView = new TestCharacterView(character, characterDrawable);
    }

    @Override
    protected void runTask() {
        
    }

    @Override
    protected void runShutdown() {

    }
}
