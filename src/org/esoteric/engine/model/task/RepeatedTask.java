package org.esoteric.engine.model.task;

/**
 * Created by russell_templet on 2/10/16.
 */
public abstract class RepeatedTask extends GameTask {

    /** A repeated task is some runnable that loops until terminated
      * The actual repeated task itself must be defined in runTask()
     */

    protected boolean terminated;
    protected long iterationCount;

    public RepeatedTask() {
        super();
    }

    public RepeatedTask(String _threadName) {
        super(_threadName);
    }

    @Override
    protected void runBody() throws InterruptedException {
        doLoopedBody();
        runAfterBody();
    }

    protected void doLoopedBody() throws InterruptedException {
        runTask();
    }

    @Override
    protected void runAfterBody() throws InterruptedException {
        if (isTerminated()) {
            runShutdown();
        } else {
            iterationCount++;
            runBody();
        }
    }

    public void setTerminated(boolean _terminated) {
        terminated = _terminated;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public long getIterationCount() {
        return iterationCount;
    }
}
