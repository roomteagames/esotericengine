package org.esoteric.engine.model.task;

/**
 * Created by russell_templet on 2/17/16.
 */
public abstract class TimedTask extends RepeatedTask {

    /**
     *  The loop sleep logic must be defined in handleSleepCondition()
     *  Optionally, logic for when and when not to run task can be defined in handleRunCondition()
     */

    protected int targetFrameTime; //in milliseconds
    protected int targetFrameRate; //in frames per second

    public TimedTask(int _targetFrameRate) {
        super();
        initTimingProperties(_targetFrameRate);
    }

    public TimedTask(int _targetFrameRate, String _threadName) {
        super(_threadName);
        initTimingProperties(_targetFrameRate);
    }

    private void initTimingProperties(int _targetFrameRate) {
        targetFrameRate = _targetFrameRate;
        targetFrameTime = 1000 / _targetFrameRate;
    }

    @Override
    protected void doLoopedBody() throws InterruptedException {
        handleRunCondition();
        long remainingFrameTime = getRemainingFrameTime();
        handleSleepCondition(remainingFrameTime);
    }

    protected void handleRunCondition() throws InterruptedException {
        runTask();
    }

    protected abstract void handleSleepCondition(long sleepTime) throws InterruptedException;

    protected long getRemainingFrameTime() {
        long endFrameTimeSinceStart = iterationCount * targetFrameTime;
        long endFrameTimeAbsolute = startTime + endFrameTimeSinceStart;
        return endFrameTimeAbsolute - System.currentTimeMillis();
    }

    protected void sleepTask(long millis) throws InterruptedException {
        Thread.sleep(millis);
    }

    public int getTargetFrameRate() {
        return targetFrameRate;
    }

    public int getTargetFrameTime() {
        return targetFrameTime;
    }
}
