package org.esoteric.engine.model.task;

/**
 * Created by russell_templet on 2/17/16.
 */
public abstract class FrameBeginTask extends TimedTask {

    /** A RepeatedTask with implemented timing logic for frame rate regulation
     * After the task body runs, the task sleeps until the expected time of its next iteration
     * If the tasks body took longer to run than its allotted time, the task will continue to sleep until the target time of the next frame
     * For more expensive, but less important game tasks, extend this class
     */

    protected boolean skipRun;

    public FrameBeginTask(int _targetFrameRate) {
        super(_targetFrameRate);
    }

    public FrameBeginTask(int _targetFrameRate, String _threadName) {
        super(_targetFrameRate, _threadName);
    }

    @Override
    protected void handleRunCondition() throws InterruptedException {
        if (skipRun) {
            skipRun = false;
        } else {
            runTask();
        }
    }

    @Override
    protected void handleSleepCondition(long sleepTime) throws InterruptedException {
        if (sleepTime > 0) {
            sleepTask(sleepTime);
        } else if (sleepTime < 0) {
            skipRun = true;
        }
    }
}
