package org.esoteric.engine.model.task;

/**
 * Created by russell_templet on 2/17/16.
 */
public abstract class FrameCatchUpTask extends TimedTask {

    /** A RepeatedTask with implemented timing logic for frame rate regulation
      * After the task body runs, the task sleeps until the expected time of its next iteration
      * If the tasks body took longer to run than its allotted time, the task runs again immediately without waiting
      * This allows the running task body to catch up to its expected run time, preventing scheduled tasks from being skipped
      * This will correct long running task bodies that occur from time to time, but will not correct constant over-time tasks
      * Expect that running task bodies with, for the most part, stay under the target frame time
      * For cheaper, but more critical game tasks, extend this class.
     */

    public FrameCatchUpTask(int _targetFrameRate) {
        super(_targetFrameRate);
    }

    public FrameCatchUpTask(int _targetFrameRate, String _threadName) {
        super(_targetFrameRate, _threadName);
    }

    @Override
    protected void handleSleepCondition(long sleepTime) throws InterruptedException {
        if (sleepTime > 0) {
            sleepTask(sleepTime);
        }
    }
}
