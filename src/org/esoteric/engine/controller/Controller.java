package org.esoteric.engine.controller;

import org.esoteric.commons.model.BaseModel;
import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.controller.params.UpdateParams;

import java.util.Observer;

/**
 * Created by russelltemplet on 7/12/16.
 */
public interface Controller<T extends BaseModel> extends Observer {

    void start();
    void updateModel(ObserverAction<T> action, ActionParams params, Entity entity);
    T getModel();
}
