package org.esoteric.engine.controller.action;

import org.esoteric.commons.model.BaseModel;
import org.esoteric.commons.model.entity.Entity;
import org.esoteric.commons.util.function.TriConsumer;
import org.esoteric.engine.controller.params.ActionParams;

/**
 * Created by russelltemplet on 9/3/16.
 */
public interface ObserverAction<T extends BaseModel> extends TriConsumer<ActionParams, T, Entity> {

    @Override
    default void accept(ActionParams params, T model, Entity entity) {
        //TODO: remove this default?
    }
}
