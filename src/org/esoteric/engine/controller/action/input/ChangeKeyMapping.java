package org.esoteric.engine.controller.action.input;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.model.InputModel;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class ChangeKeyMapping implements ObserverAction<InputModel> {

    @Override
    public void accept(ActionParams params, InputModel model, Entity entity) {

    }
}
