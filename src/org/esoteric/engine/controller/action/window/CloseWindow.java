package org.esoteric.engine.controller.action.window;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.model.WindowModel;
import org.esoteric.engine.view.container.GameWindow;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class CloseWindow implements ObserverAction<WindowModel> {

    @Override
    public void accept(ActionParams params, WindowModel model, Entity entity) {
        GameWindow window = model.getWindow();
        window.setVisible(false);
        window.dispose();
    }
}
