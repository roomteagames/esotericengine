package org.esoteric.engine.controller.action.window;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.controller.params.impl.SizeParams;
import org.esoteric.engine.model.WindowModel;
import org.esoteric.engine.view.container.GameWindow;

import java.awt.*;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class ResizeWindow implements ObserverAction<WindowModel> {

    @Override
    public void accept(ActionParams params, WindowModel model, Entity entity) {
        Dimension size = ((SizeParams) params).getSize();
        GameWindow window = model.getWindow();
        window.setSize(size);
    }
}
