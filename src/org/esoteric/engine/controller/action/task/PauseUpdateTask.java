package org.esoteric.engine.controller.action.task;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.model.TaskModel;
import org.esoteric.engine.model.task.impl.UpdateTask;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class PauseUpdateTask implements ObserverAction<TaskModel> {

    @Override
    public void accept(ActionParams params, TaskModel model, Entity entity) {
        UpdateTask task = model.getUpdateTask();
        task.getThread().suspend();
    }
}
