package org.esoteric.engine.controller.params;

import org.esoteric.commons.model.BaseModel;
import org.esoteric.engine.controller.action.ObserverAction;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class UpdateParams<T extends BaseModel> {

    private ObserverAction<T> action;
    private ActionParams actionParams;

    public UpdateParams (ObserverAction<T> _action, ActionParams _params) {
        action = _action;
        actionParams = _params;
    }

    public ObserverAction<T> getAction() {
        return action;
    }

    public ActionParams getActionParams() {
        return actionParams;
    }
}
