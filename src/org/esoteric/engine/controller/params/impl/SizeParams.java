package org.esoteric.engine.controller.params.impl;

import org.esoteric.engine.controller.params.ActionParams;

import java.awt.*;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class SizeParams implements ActionParams {

    private Dimension size;

    public SizeParams (Dimension _size) {
        size = _size;
    }

    public Dimension getSize() {
        return size;
    }
}
