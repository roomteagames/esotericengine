package org.esoteric.engine.controller.params.impl;

import org.esoteric.engine.controller.params.ActionParams;

/**
 * Created by russelltemplet on 9/3/16.
 */
public class TaskNameParams implements ActionParams {

    private String taskName;

    public TaskNameParams (String _taskName) {
        taskName = _taskName;
    }

    public String getTaskName() {
        return taskName;
    }
}
