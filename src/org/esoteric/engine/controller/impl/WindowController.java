package org.esoteric.engine.controller.impl;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.controller.params.UpdateParams;
import org.esoteric.engine.model.WindowModel;
import org.esoteric.engine.model.config.WindowConfig;

/**
 * Created by russelltemplet on 8/31/16.
 */
public class WindowController extends ConfiguredController<WindowModel, WindowConfig> {

    public WindowController (WindowConfig _config) {
        super(new WindowModel(), _config);
        config.applyAll(model);
    }

    @Override
    public void start() {

    }
}
