package org.esoteric.engine.controller.impl;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.params.UpdateParams;
import org.esoteric.engine.model.InputModel;
import org.esoteric.engine.model.config.InputConfig;
import org.esoteric.engine.view.container.GameCanvas;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by russelltemplet on 7/12/16.
 */
public class InputController extends ConfiguredController<InputModel, InputConfig> {

    private GameCanvas canvas;

    public InputController (InputConfig _config, GameCanvas _canvas) {
        super(new InputModel(), _config);
        canvas = _canvas;
    }

    @Override
    public void start() {
        SwingUtilities.invokeLater(this::createInputListener);
    }

    private void createInputListener() {
        canvas.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                config.get(e.getKeyCode())
                        .accept(model, true);
                System.out.println();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                config.get(e.getKeyCode())
                        .accept(model, false);
            }
        });
    }
}
