package org.esoteric.engine.controller.impl;

import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.model.TaskModel;
import org.esoteric.engine.model.config.TaskConfig;
import org.esoteric.engine.model.task.GameTask;

/**
 * Created by russell_templet on 2/5/16.
 */
public class TaskController extends ConfiguredController<TaskModel, TaskConfig> {

    //this controller runs game tasks as separate threads

    public TaskController(TaskConfig _config) {
        super(new TaskModel(), _config);
        config.applyAll(model);
    }

    @Override
    public void start() {
        startTask(model.getUpdateTask());
        startTask(model.getRenderTask());
        model.getAuxiliaryTasks().forEach(this::startTask);
    }

    private void startTask(GameTask task) {
        Thread taskThread = task.getThread();
        taskThread.start();
    }
}
