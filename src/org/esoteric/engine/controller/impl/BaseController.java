package org.esoteric.engine.controller.impl;

import org.esoteric.commons.model.BaseModel;
import org.esoteric.commons.model.entity.Entity;
import org.esoteric.engine.controller.Controller;
import org.esoteric.engine.controller.action.ObserverAction;
import org.esoteric.engine.controller.params.ActionParams;
import org.esoteric.engine.controller.params.UpdateParams;

import java.util.Observable;

/**
 * Created by russelltemplet on 7/12/16.
 */
public abstract class BaseController<T extends BaseModel> implements Controller<T> {

    protected T model;

    public BaseController(T _model) {
        model = _model;
    }

    @Override
    public void update(Observable o, Object arg) {
        Entity entity = (Entity) o;
        UpdateParams<T> params = (UpdateParams<T>) arg;

        ObserverAction<T> action = params.getAction();
        ActionParams actionParams = params.getActionParams();

        updateModel(action, actionParams, entity);
    }

    @Override
    public void updateModel(ObserverAction<T> action, ActionParams params, Entity entity) {
        action.accept(params, model, entity);
    }

    @Override
    public T getModel() {
        return model;
    }
}