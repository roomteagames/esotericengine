package org.esoteric.engine.controller.impl;

import org.esoteric.commons.model.BaseModel;
import org.esoteric.engine.model.config.Config;

/**
 * Created by russelltemplet on 8/28/16.
 */
public abstract class ConfiguredController<T extends BaseModel, U extends Config> extends BaseController<T> {

    protected U config;

    public ConfiguredController (T _model, U _config) {
        super(_model);
        config = _config;
    }
}
