package org.esoteric.engine.controller.impl;

import org.esoteric.engine.model.EntityModel;
import org.esoteric.engine.model.config.EntityConfig;

/**
 * Created by russelltemplet on 8/29/16.
 */

public class EntityController extends ConfiguredController<EntityModel, EntityConfig> {

    //TODO: what goes here???

    public EntityController (EntityConfig _config) {
        super(new EntityModel(), _config);
    }

    @Override
    public void start() {

    }
}
