package org.esoteric.engine.main.impl;

import org.esoteric.engine.controller.impl.EntityController;
import org.esoteric.engine.controller.impl.InputController;
import org.esoteric.engine.controller.impl.TaskController;
import org.esoteric.engine.controller.impl.WindowController;
import org.esoteric.engine.main.Engine;
import org.esoteric.engine.model.InputModel;
import org.esoteric.engine.model.TaskModel;
import org.esoteric.engine.model.WindowModel;
import org.esoteric.engine.model.config.*;
import org.esoteric.engine.model.task.impl.RenderTask;
import org.esoteric.engine.model.task.impl.UpdateTask;
import org.esoteric.engine.view.container.GameCanvas;
import org.esoteric.engine.view.container.GameWindow;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by russelltemplet on 7/12/16.
 */
public class EsotericEngine implements Engine {

    //todo: this needs to interface with user code or read config files to get config settings

    public static final int DEFAULT_UPDATE_FRAMERATE = 100;
    public static final int DEFAULT_RENDER_FRAMERATE = 60;

    private WindowController windowController;
    private TaskController taskController;
    private InputController inputController;
    private EntityController entityController;

    private EngineConfig config;

    /*public EsotericEngine () {
        this();
    }*/

    public EsotericEngine (EngineConfig _config) {
        config = _config;

        build();
    }

    public void build() {
        buildWindowController();
        buildTaskController();
        buildEntityController();
        buildInputController();
    }

    public void start() {
        windowController.start();
        taskController.start();
        entityController.start();
        inputController.start();
    }

    private void buildWindowController() {
        windowController = new WindowController(config.getWindowConfig());
    }

    private void buildTaskController() {
        taskController = new TaskController(config.getTaskConfig());
    }

    private void buildEntityController() {
        entityController = new EntityController(config.getEntityConfig());
    }

    private void buildInputController() {
        GameCanvas canvas = windowController.getModel().getCanvas();
        inputController = new InputController(config.getInputConfig(), canvas);
    }
}
