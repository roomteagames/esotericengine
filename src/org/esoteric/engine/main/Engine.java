package org.esoteric.engine.main;

/**
 * Created by russelltemplet on 6/28/16.
 */
public interface Engine {

    void build();
    void start();


}
