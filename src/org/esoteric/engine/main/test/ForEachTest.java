package org.esoteric.engine.main.test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by russelltemplet on 8/28/16.
 */
public class ForEachTest {

    public static void main (String[] args) {

        List<Integer> list = Arrays.asList(1, 8, 2, 4, 9, 3, 6, 5, 3);
        Predicate<Integer> condition = e -> e <= 2;

        long count = countFulfillingElements(list, condition);
        System.out.println("Num of fulfilling elements: " + count);
    }

    private static <E> long countFulfillingElements(Collection<E> collection, Predicate<E> predicate) {
        return collection
                .stream()
                .filter(predicate)
                .count();
    }
}
