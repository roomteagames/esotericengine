package org.esoteric.engine.main.test;

import org.esoteric.engine.main.Engine;
import org.esoteric.engine.main.impl.EsotericEngine;
import org.esoteric.engine.model.config.EngineConfig;

/**
 * Created by russelltemplet on 9/4/16.
 */
public class EngineTest {

    public static void main (String[] args) {
        EngineConfig config = new EngineConfig();
        Engine engine = new EsotericEngine(config);

        engine.start();
    }
}
