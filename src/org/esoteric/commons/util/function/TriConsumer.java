package org.esoteric.commons.util.function;

/**
 * Created by russelltemplet on 9/3/16.
 */

@FunctionalInterface
public interface TriConsumer<T, U, V> {

    void accept(T arg1, U arg2, V arg3);
}