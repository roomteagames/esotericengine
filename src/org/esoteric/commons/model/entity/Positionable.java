package org.esoteric.commons.model.entity;

import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 5/31/16.
 */
public interface Positionable {

    Vector2 getPosition();
}
