package org.esoteric.commons.model.entity;

/**
 * Created by russelltemplet on 6/26/16.
 */

@FunctionalInterface
public interface Updateable {

    void update();
}
