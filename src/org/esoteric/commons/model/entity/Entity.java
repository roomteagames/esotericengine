package org.esoteric.commons.model.entity;

import org.esoteric.commons.math.vector.Vector2;
import org.esoteric.commons.model.BaseModel;

/**
 * Created by russell_templet on 2/18/16.
 */
public abstract class Entity extends BaseModel implements Updateable {

    protected Physics physics;

    public Entity () {}

    public Entity (Physics _physics) {
        physics = _physics;
    }

    public Physics getPhysics() {
        return physics;
    }

    public void setPhysics(Physics _physics) {
        physics = _physics;
    }

    public Vector2 getPosition() {
        return physics.getPosition();
    }
}
