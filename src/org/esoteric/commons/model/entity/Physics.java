package org.esoteric.commons.model.entity;

import org.esoteric.commons.math.vector.MagnitudeVector2;
import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russelltemplet on 8/26/16.
 */
public abstract class Physics {

    protected Vector2 position;
    protected Vector2 velocity;
    protected Vector2 acceleration;

    protected Vector2 previousPosition;
    protected Vector2 previousVelocity;
    protected Vector2 previousAcceleration;

    public Physics () {}

    public Physics (Vector2 _position) {
        this(_position, MagnitudeVector2.zero());
    }

    public Physics (Vector2 _position, Vector2 _velocity) {
        this(_position, _velocity, MagnitudeVector2.zero());
    }

    public Physics (Vector2 _position, Vector2 _velocity, Vector2 _acceleration) {
        position = _position;
        velocity = _velocity;
        acceleration = _acceleration;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 _position) {
        position = _position;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 _velocity) {
        velocity = _velocity;
    }

    public Vector2 getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Vector2 _acceleration) {
        acceleration = _acceleration;
    }

    public Vector2 getPreviousPosition() {
        return previousPosition;
    }

    public Vector2 getPreviousVelocity() {
        return previousVelocity;
    }

    public Vector2 getPreviousAcceleration() {
        return previousAcceleration;
    }
}
