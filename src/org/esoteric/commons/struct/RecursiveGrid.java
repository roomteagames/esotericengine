package org.esoteric.commons.struct;

import org.esoteric.commons.model.entity.Positionable;
import org.esoteric.commons.math.vector.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by russell_templet on 5/31/16.
 */
public class RecursiveGrid<T extends Positionable> {

    //todo: maybe a non-recursive grid would be more efficient...

    //todo: figure out which type of data struct would make this concept most efficient
    private List<T> items;
    private RecursiveGrid[][] innerGrids;
    private int dimension;
    private int width;
    private int height;
    private int depth;
    private Vector2 offset;

    public RecursiveGrid(int _dimension, int _width, int _height, int _depth) {
        this(_dimension, _width, _height, _depth, Vector2.zero());
    }

    public RecursiveGrid(int _dimension, int _width, int _height, int _depth, Vector2 _offset) {
        dimension = _dimension;
        width = _width;
        height = _height;
        depth = _depth;
        offset = _offset;

        items = new ArrayList<>();
        innerGrids = new RecursiveGrid[dimension][dimension];

        initInnerGrids();
    }

    protected void initInnerGrids() {
        if (depth > 1) {
            for (int i = 0; i < dimension; i++) {
                for (int j = 0; j < dimension; j++) {
                    Vector2 currentOffset = new Vector2(
                            offset.getI() + j * width / dimension,
                            offset.getJ() + i * height / dimension
                    );
                    innerGrids[i][j] = constructGrid(depth - 1, currentOffset);
                }
            }
        }
    }

    //Override to construct subclasses of RecursiveGrid
    protected RecursiveGrid<T> constructGrid(int _depth, Vector2 _offset) {
        return new RecursiveGrid<>(
            dimension,
            width / dimension,
            height / dimension,
            _depth,
            _offset
        );
    }

    public void populateGrid(List<T> _items) {
        items = _items;
        for (T item : items) {
            handleInsertItem(item);
        }
    }

    public void insert(T t) {
        items.add(t);
        handleInsertItem(t);
    }

    protected void handleInsertItem(T item) {
        Vector2 relativePosition = item.getPosition().minus(offset);
        if (dimension > 1) {
            int xIndex = (int) relativePosition.getI() / (width / dimension);
            int yIndex = (int) relativePosition.getJ() / (width / dimension);
            RecursiveGrid<T> subgrid = innerGrids[yIndex][xIndex];
            subgrid.insert(item);
        }
    }


    public void afterItemMoved(int index) {
        T item = items.get(index);
        //todo: brainstorm this more before coding it
    }

    public List<T> getItems() {
        return items;
    }

    public RecursiveGrid[][] getInnerGrids() {
        return innerGrids;
    }

    public int getDimension() {
        return dimension;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public Vector2 getOffset() {
        return offset;
    }
}
