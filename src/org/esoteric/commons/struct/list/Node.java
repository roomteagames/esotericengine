package org.esoteric.commons.struct.list;

import org.esoteric.kinetics.model.entity.ListNodeKnowledgeable;

/**
 * Created by russell_templet on 6/5/16.
 */
public class Node<T extends ListNodeKnowledgeable> {
    //todo: once we remove ListNodeKnowledgeable, should be any T

    private T item;
    private Node previous;
    private Node next;

    public Node(T _item) {
        setItem(_item);
    }

    public T getItem() {
        return item;
    }

    public void setItem(T _item) {
        if (item != null) {
            item.setListNode(null);
        }
        item = _item;
        updateNodeKnowledge();
    }

    public boolean hasPrevious() {
        return previous != null;
    }

    public Node getPrevious() {
        return previous;
    }

    public void setPrevious(Node _previous) {
        previous = _previous;
    }

    public boolean hasNext() {
        return next != null;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    private void updateNodeKnowledge() {
        item.setListNode(this);
    }
}
