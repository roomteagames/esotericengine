package org.esoteric.commons.struct.list;

import org.esoteric.kinetics.model.entity.ListNodeKnowledgeable;

import java.util.Iterator;
import java.util.List;

/**
 * Created by russell_templet on 6/5/16.
 */
public class KineticsLinkedList<T extends ListNodeKnowledgeable> implements Iterable<Node> {
    //todo: once we remove ListNodeKnowledgeable, should be any T

    private Node head;
    private Node tail;
    private int length;

    public KineticsLinkedList() {}

    public KineticsLinkedList(List<T> _items) {
        //todo: implement this...
    }

    public void add(T _item) {
        Node addedNode = new Node<>(_item);
        if (length > 0) {
            tail.setNext(addedNode);
            addedNode.setPrevious(tail);
        } else {
            head = addedNode;
        }
        tail = addedNode;
        length++;
    }

    public void addFirst(T _item) {
        Node addedNode = new Node<>(_item);
        if (length > 0) {
            head.setPrevious(addedNode);
            addedNode.setNext(head);
        } else {
            tail = addedNode;
        }
        head = addedNode;
        length++;
    }

    //todo: it probably isn't good practice to store node reference in the item...
    //todo: an unauthorized edit can mess up this list structure
    public boolean remove(T _item) {
        boolean found = false;
        Node knownNode = _item.getListNode();

        if (knownNode != null) {
            Node nextNode = knownNode.getNext();
            Node previousNode = knownNode.getPrevious();

            if (knownNode.hasPrevious()) {
                previousNode.setNext(nextNode);
            }
            if (knownNode.hasNext()) {
                nextNode.setPrevious(previousNode);
            }
            _item.setListNode(null);
            found = true;
        }
        return found;
    }

    public void remove(Node node) {
        Node previousNode = node.getPrevious();
        Node nextNode = node.getNext();
        if (node.hasPrevious()) {
            previousNode.setNext(nextNode);
        }
        if (node.hasNext()) {
            nextNode.setPrevious(previousNode);
        }
        node.getItem().setListNode(null);   //todo: remove when we remote node ref in item
    }

    @Override
    public Iterator<Node> iterator() {
        return new Iterator<Node>() {
            private Node currentNode;

            @Override
            public boolean hasNext() {
                boolean atBeginning = currentNode == null && head != null;
                boolean inProgress = currentNode != null && currentNode.hasNext();
                return atBeginning || inProgress;
            }

            @Override
            public Node next() {
                if (currentNode == null) {
                    currentNode = head;
                } else {
                    currentNode = currentNode.getNext();
                }
                return currentNode;
            }
        };
    }
}
