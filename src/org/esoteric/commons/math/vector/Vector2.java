package org.esoteric.commons.math.vector;

/**
 * Created by russell_templet on 4/10/16.
 */
public class Vector2 {

    private float i;
    private float j;

    public Vector2(float _i, float _j) {
        i = _i;
        j = _j;
    }

    public double getMagnitude() {
        return Math.sqrt(getMagnitudeSquared());
    }

    public double getMagnitudeSquared() {
        return i*i + j*j;
    }


    // Vector math that produces a new vector

    public Vector2 plus(Vector2 other) {
        float iSum = i + other.getI();
        float jSum = j + other.getJ();
        return constructNewVector(iSum, jSum);
    }

    public Vector2 plusX(float x) {
        return constructNewVector(i + x, j);
    }

    public Vector2 plusY(float y) {
        return constructNewVector(i, j + y);
    }

    public Vector2 minus(Vector2 other) {
        float iDiff = i - other.getI();
        float jDiff = j - other.getJ();
        return constructNewVector(iDiff, jDiff);
    }

    public Vector2 minusX(float x) {
        return constructNewVector(i - x, j);
    }

    public Vector2 minusY(float y) {
        return constructNewVector(i, j - y);
    }

    public Vector2 getScaled(float scalar) {
        float iProduct = scalar * i;
        float jProduct = scalar * j;
        return constructNewVector(iProduct, jProduct);
    }

    public Vector2 getDivided(float divisor) {
        float iQuotient = i / divisor;
        float jQuotient = j / divisor;
        return constructNewVector(iQuotient, jQuotient);
    }

    public Vector2 getNormalized() {
        float magnitude = (float) getMagnitude();
        return getDivided(magnitude);
    }

    protected Vector2 constructNewVector(float _i, float _j) {
        return new Vector2(_i, _j);
    }


    // Vector math that edits the vector

    public void add(Vector2 other) {
        i += other.getI();
        j += other.getJ();
        onAfterUpdate();
    }

    public void addX(float x) {
        i += x;
        onAfterUpdate();
    }

    public void addY(float y) {
        j += y;
        onAfterUpdate();
    }

    public void subtract(Vector2 other) {
        i -= other.getI();
        j -= other.getJ();
        onAfterUpdate();
    }

    public void subtractX(float x) {
        i -= x;
        onAfterUpdate();
    }

    public void subtractY(float y) {
        j -= y;
        onAfterUpdate();
    }

    public void scale(float scalar) {
        i *= scalar;
        j *= scalar;
        onAfterUpdate();
    }

    public void divide(float divisor) {
        i /= divisor;
        j /= divisor;
        onAfterUpdate();
    }

    public void normalize() {
        float magnitude = (float) getMagnitude();
        divide(magnitude);
        onAfterUpdate();
    }

    protected void onAfterUpdate() { }


    public float dot(Vector2 other) {
        float iProduct = i * other.getI();
        float jProduct = j * other.getJ();
        return iProduct + jProduct;
    }

    public float getI() {
        return i;
    }

    public float getJ() {
        return j;
    }

    public int getIntI() {
        return (int) i;
    }

    public int getIntJ() {
        return (int) j;
    }

    @Override
    public boolean equals (Object o) {
        boolean isEqual = false;
        if (o instanceof Vector2) {
            Vector2 v = (Vector2) o;
            isEqual = (i == v.getI() && j == v.getJ());
        }
        return isEqual;
    }


    //static methods

    public static Vector2 zero() {
        return new Vector2 (0, 0);
    }

    public static Vector2 one() {
        return new Vector2 (1, 1);
    }
}
