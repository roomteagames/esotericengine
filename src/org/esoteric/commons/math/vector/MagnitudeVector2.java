package org.esoteric.commons.math.vector;

/**
 * Created by russell_templet on 4/11/16.
 */
public class MagnitudeVector2 extends Vector2 {

    private double magnitude;

    //This vector automatically calculates magnitude on construction

    public MagnitudeVector2(float _i, float _j) {
        super(_i, _j);
        magnitude = super.getMagnitude();
    }

    public double getMagnitude() {
        return magnitude;
    }

    public double getMagnitudeSquared() {
        return magnitude * magnitude;
    }

    @Override
    protected Vector2 constructNewVector(float _i, float _j) {
        return new MagnitudeVector2(_i, _j);
    }

    @Override
    protected void onAfterUpdate() {
        magnitude = super.getMagnitude();
    }


    //static methods

    public static MagnitudeVector2 zero() {
        return new MagnitudeVector2 (0, 0);
    }

    public static MagnitudeVector2 one() {
        return new MagnitudeVector2 (1, 1);
    }
}
