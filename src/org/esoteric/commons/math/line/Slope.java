package org.esoteric.commons.math.line;

import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 4/12/16.
 */
public class Slope {

    private double value;
    private double rise;
    private Double run;
    private boolean defined;

    public Slope(Vector2 _a, Vector2 _b) {
        this(_b.getJ(), _a.getJ(), _b.getI(), _a.getI());
    }

    public Slope(double _by, double _ay, double _bx, double _ax) {
        this(_by - _ay, _bx - _ax);
    }

    public Slope(double _rise, double _run) {
        rise = _rise;
        run = _run;
        defined = run != 0;
        if (defined) {
            value = rise / run;
        }
    }

    public Double getValue() {
        return value;
    }

    public double getRise() {
        return rise;
    }

    public double getRun() {
        return run;
    }

    public boolean isDefined() {
        return defined;
    }
}
