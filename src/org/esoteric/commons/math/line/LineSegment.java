package org.esoteric.commons.math.line;

import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 4/12/16.
 */
public class LineSegment {

    private Vector2 a;
    private Vector2 b;

    public LineSegment(Vector2 _a, Vector2 _b) {
        a = _a;
        b = _b;
    }

    public void translate(Vector2 translation) {
        a.add(translation);
        b.add(translation);
    }

    public double getLength() {
        Vector2 diffVector = b.minus(a);
        return diffVector.getMagnitude();
    }

    public Slope getSlope() {
        return new Slope(a, b);
    }

    public Vector2 getA() {
        return a;
    }

    public Vector2 getB() {
        return b;
    }

    public void setA(Vector2 a) {
        this.a = a;
    }

    public void setB(Vector2 b) {
        this.b = b;
    }
}
