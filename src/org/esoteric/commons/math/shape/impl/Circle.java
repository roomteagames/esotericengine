package org.esoteric.commons.math.shape.impl;

import org.esoteric.commons.math.shape.Shape;
import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 4/12/16.
 */
public class Circle extends Shape {

    private Vector2 center;
    private float radius;

    public Circle(Vector2 _center, float _radius) {
        center = _center;
        radius = _radius;
    }

    public Vector2 getCenter() {
        return center;
    }

    public float getRadius() {
        return radius;
    }

    public float getDiameter() {
        return radius * 2;
    }

    public double getCircumference() {
        return 2 * Math.PI * radius;
    }

    protected void handleRotation(float radians) { }
}
