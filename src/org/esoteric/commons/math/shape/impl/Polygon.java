package org.esoteric.commons.math.shape.impl;

import org.esoteric.commons.math.shape.Shape;
import org.esoteric.commons.math.shape.Side;
import org.esoteric.commons.math.vector.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by russell_templet on 4/12/16.
 */
public class Polygon extends Shape {

    protected List<Side> sides;
    protected List<Vector2> vertices;

    public Polygon() {
        sides = new ArrayList<>();
        vertices = new ArrayList<>();
    }

    public Polygon(List<Vector2> _vertices, List<Float> _dragList) {
        this();
        vertices = _vertices;
        createSides(_dragList);
    }

    // todo: should rotation by applied immediately or after sides are placed?
    public Polygon(float _rotation) {
        super(_rotation);
        sides = new ArrayList<>();
    }

    public void createSides(List<Float> _dragList) {
        int lastIndex = vertices.size() - 1;
        for (int i = 0; i < lastIndex; i++) {
            sides.add(new Side(
                    vertices.get(i),
                    vertices.get(i + 1),
                    _dragList.get(i)));
        }
        sides.add(new Side(
                vertices.get(lastIndex),
                vertices.get(0),
                _dragList.get(lastIndex)));
    }

    public void addVertex(Vector2 v) {
        addVertex(v, 0);
    }

    public void addVertex(Vector2 v, float drag) {
        vertices.add(v);
        int vertexCount = vertices.size();
        if (vertexCount > 1) {
            Side oldClosingSide = sides.get(vertexCount - 2);
            oldClosingSide.setB(v);    //no longer closing the polygon
        }
        Side newClosingSide = new Side(v, vertices.get(0), drag);
        sides.add(newClosingSide);  //new side closing the polygon
    }

    public void insertVertex(int index, Vector2 v) {
        insertVertex(index, v, 0);
    }

    public void insertVertex(int index, Vector2 v, float drag) throws IndexOutOfBoundsException {
        if (vertices.size() > 0) {
            Vector2 oldVertexAtIndex = vertices.get(index);
            vertices.add(index, v);
            Side oldSide;
            if (index > 0) {
                oldSide = sides.get(index - 1);
            } else {
                oldSide = sides.get(sides.size() - 1);
            }
            oldSide.setB(v);
            Side newSide = new Side(v, oldVertexAtIndex, drag);
            sides.add(newSide);
        } else {
            addVertex(v, drag);
        }
    }

    public void deleteVertex(int index) throws IndexOutOfBoundsException {
        vertices.remove(index);
        int vertexCount = vertices.size();
        if (vertexCount > 0) {
            Vector2 newBVertex;
            if (index == 0) {
                newBVertex = vertices.get(index);
                sides.get(vertexCount - 1).setB(newBVertex);
            } else if (index < vertexCount - 1) {
                newBVertex = vertices.get(index);
                sides.get(index - 1).setB(newBVertex);
            } else {
                newBVertex = vertices.get(0);
                sides.get(index - 1).setB(newBVertex);
            }
        }
        sides.remove(index);
    }

    public void translateVertex(int index, Vector2 translation) {
        Vector2 vertex = vertices.get(index);
        vertex.add(translation);
    }

    public float getPerimeter() {
        float perimeter = 0;
        for (Side side : sides) {
            perimeter += side.getLength();
        }
        return perimeter;
    }

    public Side getSide(int index) {
        return sides.get(index);
    }

    public List<Side> getSides() {
        return sides;
    }

    public Vector2 getVertex(int index) {
        return vertices.get(index);
    }

    public List<Vector2> getVertices() {
        return vertices;
    }

    public boolean isClosed() {
        return sides.size() >= 3;
    }

    protected void handleRotation(float radians) {
        //todo: base rotation off of center of shape or center of mass???
    }
}
