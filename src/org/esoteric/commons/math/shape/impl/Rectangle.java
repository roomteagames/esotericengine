package org.esoteric.commons.math.shape.impl;

import org.esoteric.commons.math.vector.Vector2;

import java.util.Arrays;
import java.util.List;

/**
 * Created by russell_templet on 4/12/16.
 */
public class Rectangle extends Polygon {

    private Vector2 topLeft;
    private float length;
    private float height;

    public Rectangle(Vector2 _topLeft, float _length, float _height, List<Float> _dragList) {
        super(
                Arrays.asList(
                        _topLeft,
                        _topLeft.plusX(_length),
                        _topLeft.plus(new Vector2(_length, _height)),
                        _topLeft.plusY(_height)),
                _dragList);
        topLeft = _topLeft;
    }

    public Rectangle(Vector2 _topLeft, float _length, float _height, float _rotation) {
        super(_rotation);
        topLeft = _topLeft;

    }
}
