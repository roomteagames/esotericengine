package org.esoteric.commons.math.shape;

import org.esoteric.commons.math.line.LineSegment;
import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 4/12/16.
 */
public class Side extends LineSegment {

    private float drag;

    public Side(Vector2 _a, Vector2 _b, float _drag) {
        super(_a, _b);
        drag = _drag;
    }

    public float getDrag() {
        return drag;
    }

    public void setDrag(float drag) {
        this.drag = drag;
    }
}
