package org.esoteric.commons.math.shape;

/**
 * Created by russell_templet on 4/12/16.
 */
public abstract class Shape {

    protected float rotation;   //in radians

    public Shape() {}

    public Shape(float _rotation) {
        rotation = _rotation;
    }

    public float getRotation() {
        return rotation;
    }

    public void rotate(float radians) {
        rotation += radians;
        handleRotation(radians);
    }

    protected abstract void handleRotation(float radians);
}
