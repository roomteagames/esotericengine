package org.esoteric.kinetics.controller.force;

import org.esoteric.kinetics.controller.Controller;
import org.esoteric.kinetics.model.force.ForceRegistration;
import org.esoteric.kinetics.model.force.generator.ForceGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by russell_templet on 4/17/16.
 */
public class ForceController implements Controller {

    private List<ForceRegistration> forceRegistry;

    public ForceController() {
        forceRegistry = new ArrayList<>();
    }

    public void addForceRegistration(ForceRegistration registration) {
        forceRegistry.add(registration);
    }

    public void removeForceRegistration(ForceRegistration registration) {
        forceRegistry.remove(registration);
    }

    public void removeForceRegistrationAt(int index) {
        forceRegistry.remove(index);
    }

    @Override
    public void update(double duration) {
        for (ForceRegistration registration : forceRegistry) {
            ForceGenerator generator = registration.getForceGenerator();
            generator.updateForce(registration.getEntity(), duration);
        }
    }
}
