package org.esoteric.kinetics.controller;

/**
 * Created by russell_templet on 4/17/16.
 */
public interface Controller {

    void update(double duration);
}
