package org.esoteric.kinetics.model.collision.task.impl.connection;

import org.esoteric.kinetics.model.collision.Contact;
import org.esoteric.kinetics.model.entity.KineticsPhysics;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by russell_templet on 4/11/16.
 */
public class Rod extends Connection {

    public Rod(float _length, float _restitution) {
        super(_length, _restitution);
    }

    @Override
    public Contact generateContact(KineticsPhysics a, KineticsPhysics b) {
        throw new NotImplementedException();
    }
}