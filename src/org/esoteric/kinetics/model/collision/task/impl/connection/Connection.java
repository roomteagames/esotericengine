package org.esoteric.kinetics.model.collision.task.impl.connection;

import org.esoteric.kinetics.model.collision.task.CollisionTask;

/**
 * Created by russell_templet on 4/10/16.
 */
public abstract class Connection implements CollisionTask {

    protected float length;
    protected float restitution;

    public Connection(float _length, float _restitution) {
        length = _length;
        restitution = _restitution;
    }

    public float getLength() {
        return length;
    }

    public float getRestitution() {
        return restitution;
    }
}
