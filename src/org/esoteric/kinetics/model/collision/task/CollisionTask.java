package org.esoteric.kinetics.model.collision.task;

import org.esoteric.kinetics.model.collision.Contact;
import org.esoteric.kinetics.model.entity.KineticsPhysics;

/**
 * Created by russell_templet on 4/10/16.
 */
public interface CollisionTask {

    Contact generateContact(KineticsPhysics a, KineticsPhysics b);
}
