package org.esoteric.kinetics.model.entity.impl;

import org.esoteric.kinetics.model.entity.KineticsPhysics;
import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 4/17/16.
 */
public class Particle extends KineticsPhysics {

    public Particle (float _mass) {
        super(_mass);
    }

    public Particle (float _mass, Vector2 _position) {
        super(_mass, _position);
    }

    public Particle (float _mass, Vector2 _position, Vector2 _velocity) {
        super(_mass, _position, _velocity);
    }
}
