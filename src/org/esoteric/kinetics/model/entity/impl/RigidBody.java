package org.esoteric.kinetics.model.entity.impl;

import org.esoteric.kinetics.model.entity.KineticsPhysics;
import org.esoteric.commons.math.shape.Shape;
import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 4/17/16.
 */
public class RigidBody extends KineticsPhysics {

    protected Shape shape;

    public RigidBody (float _mass) {
        super(_mass);
    }

    public RigidBody (float _mass, Vector2 _position) {
        super(_mass, _position);
    }

    public RigidBody (float _mass, Vector2 _position, Vector2 _velocity) {
        super(_mass, _position, _velocity);
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape _shape) {
        shape = _shape;
    }
}
