package org.esoteric.kinetics.model.entity;

import org.esoteric.commons.struct.list.Node;

/**
 * Created by russell_templet on 6/5/16.
 */
public interface ListNodeKnowledgeable {
    //todo: deprecated -- probably a bad practice

    void setListNode(Node node);
    Node getListNode();
}
