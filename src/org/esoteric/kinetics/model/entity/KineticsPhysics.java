package org.esoteric.kinetics.model.entity;

import org.esoteric.commons.math.vector.MagnitudeVector2;
import org.esoteric.commons.math.vector.Vector2;
import org.esoteric.commons.model.entity.Entity;
import org.esoteric.commons.model.entity.Physics;
import org.esoteric.commons.model.entity.Positionable;
import org.esoteric.commons.struct.list.Node;

/**
 * Created by russell_templet on 4/10/16.
 */
public abstract class KineticsPhysics extends Physics implements ListNodeKnowledgeable {
    //todo: remove the concept of ListNodeKnowledgeable

    protected Vector2 force;
    protected Vector2 previousVelocity;
    protected Vector2 previousAcceleration;
    protected Vector2 previousForce;

    protected float mass;

    protected Node listNode;

    public KineticsPhysics(float _mass) {
        mass = _mass;
        resetIntegratedVectors();
        initPreviousVectors();
    }

    public KineticsPhysics(float _mass, Vector2 _position) {
        super(_position);
        mass = _mass;
        resetIntegratedVectors();
        initPreviousVectors();
    }

    public KineticsPhysics(float _mass, Vector2 _position, Vector2 _velocity) {
        super(_position, _velocity);
        mass = _mass;
        resetIntegratedVectors();
        initPreviousVectors();
    }

    public boolean isInfiniteMass() {
        return mass == 0;
    }

    public void accumulateForce(Vector2 _force) {
        force.add(_force);
    }

    public void integrateForce() {
        previousForce = force;
        previousAcceleration = acceleration;
        previousVelocity = velocity;
        previousPosition = position;

        acceleration = force.getDivided(mass);
        velocity.add(acceleration);
        position.add(velocity);
        resetIntegratedVectors();
    }

    private void initPreviousVectors() {
        previousPosition = Vector2.zero();
        previousVelocity = Vector2.zero();
        previousAcceleration = MagnitudeVector2.zero();
        previousForce = MagnitudeVector2.zero();
    }

    private void resetIntegratedVectors() {
        acceleration = MagnitudeVector2.zero();
        force = MagnitudeVector2.zero();
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public Vector2 getAcceleration() {
        return acceleration;
    }

    public Vector2 getForce() {
        return force;
    }

    public Vector2 getPreviousPosition() {
        return previousPosition;
    }

    public Vector2 getPreviousVelocity() {
        return previousVelocity;
    }

    public Vector2 getPreviousAcceleration() {
        return previousAcceleration;
    }

    public Vector2 getPreviousForce() {
        return previousForce;
    }

    public float getMass() {
        return mass;
    }

    @Override
    public void setListNode(Node _listNode) {
        listNode = _listNode;
    }

    @Override
    public Node getListNode() {
        return listNode;
    }
}
