package org.esoteric.kinetics.model.force;

import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 5/26/16.
 */
public class Gravity {

    private String name;
    private Vector2 acceleration;

    public Gravity(String _name, Vector2 _acceleration) {
        name = _name;
        acceleration = _acceleration;
    }

    public Gravity(String _name, float _x, float _y) {
        name = _name;
        acceleration = new Vector2(_x, _y);
    }

    public String getName() {
        return name;
    }

    public Vector2 getAcceleration() {
        return acceleration;
    }
}
