package org.esoteric.kinetics.model.force.generator.impl;

import org.esoteric.kinetics.model.entity.KineticsPhysics;
import org.esoteric.kinetics.model.force.generator.ForceGenerator;
import org.esoteric.commons.math.vector.Vector2;

/**
 * Created by russell_templet on 4/17/16.
 */
public class GravityForceGenerator implements ForceGenerator {

    private Vector2 gravityAcceleration;

    public GravityForceGenerator (Vector2 _gravityAcceleration) {
        gravityAcceleration = _gravityAcceleration;
    }

    @Override
    public void updateForce(KineticsPhysics entity, double duration) {
        if (!entity.isInfiniteMass()) {
            Vector2 gravityForce = gravityAcceleration.getScaled(entity.getMass());
            entity.accumulateForce(gravityForce);
        }
    }
}
