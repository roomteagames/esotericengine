package org.esoteric.kinetics.model.force.generator;

import org.esoteric.kinetics.model.entity.KineticsPhysics;

/**
 * Created by russell_templet on 4/17/16.
 */
public interface ForceGenerator {

    void updateForce(KineticsPhysics entity, double duration);
}
