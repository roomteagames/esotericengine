package org.esoteric.kinetics.model.force;

import org.esoteric.kinetics.model.entity.KineticsPhysics;
import org.esoteric.kinetics.model.force.generator.ForceGenerator;

/**
 * Created by russell_templet on 4/17/16.
 */
public class ForceRegistration {

    private KineticsPhysics entity;
    private ForceGenerator forceGenerator;

    public ForceRegistration(KineticsPhysics _entity, ForceGenerator _forceGenerator) {
        entity = _entity;
        forceGenerator = _forceGenerator;
    }

    public KineticsPhysics getEntity() {
        return entity;
    }

    public ForceGenerator getForceGenerator() {
        return forceGenerator;
    }
}
